package com.example.mysampleapp;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

public class MyAppWidgetConfigureActivity extends Activity {

    private static final String TAG = "ConfigActivity";

    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    View.OnClickListener mAddButtonOnClickListener = v -> {
        final Context context = MyAppWidgetConfigureActivity.this;

        // It is the responsibility of the configuration activity to update the app widget
        // but initialisation of the new widget should happen via a trigger from onUpdate()
        // so no need to do that here too...
        /*
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            MyAppWidget.setupAppWidget(context, appWidgetManager, mAppWidgetId);
        */

        EditText editText = this.findViewById(R.id.delay);
        String str = String.valueOf(editText.getText());
        long delay = Long.parseLong(str);
        // restrict range of delay (currently in mins)...
        delay = (delay < 0) ? 0 : delay;
        delay = (delay > 10) ? 10 : delay;
        // convert to ms from mins...
        delay = delay * 60 * 1000;

        // initialise widget, which will also schedule both periodic and immediate work...
        MyAppWidget.initialiseAppWidget(context, mAppWidgetId, MyAppWidget.CONFIG_ACTIVITY_TRIGGER, delay);

        // Make sure we pass back the original appWidgetId
        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_OK, resultValue);
        finish();
    };

    View.OnClickListener mLogCatButtonOnClickListener = v -> {
        final Context context = MyAppWidgetConfigureActivity.this;
        logcat(context);
    };

    public MyAppWidgetConfigureActivity() {
        super();
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED);

        setContentView(R.layout.my_app_widget_configure);
        findViewById(R.id.add_button).setOnClickListener(mAddButtonOnClickListener);
        findViewById(R.id.logcat_button).setOnClickListener(mLogCatButtonOnClickListener);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    static File getStorageLocation(Context context) {
        return context.getExternalFilesDir(null);
    }

    private void logcat(Context context) {
        if ( isExternalStorageWritable() ) {
            File logStorageLocation = getStorageLocation(context);
            if (logStorageLocation == null) {
                Log.d(TAG, "problem getting logStorageLocation");
                return;
            }
            Log.d(TAG, "logStorageLocation path: " + logStorageLocation.getAbsolutePath());
            try {
                long id = System.currentTimeMillis();
                String logFileName = "logcat-" + id + ".txt";
                File logFile = new File(logStorageLocation, logFileName);
                String cmd = "logcat -v time -f " + logFile + " *:V";
                Log.d(TAG, "running command: " + cmd);
                Runtime.getRuntime().exec(cmd);
                String logFileShortPath = logFile.getPath().replace("/storage/emulated/0", "");
                String msg = "written logcat to " + logFileShortPath;
                ((TextView) findViewById(R.id.logcat_location)).setText(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(TAG, "not isExternalStorageWritable not accessible");
        }
    }
}

package com.example.mysampleapp;

import android.app.PendingIntent;
import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link MyAppWidgetConfigureActivity MyAppWidgetConfigureActivity}
 */
public class MyAppWidget extends AppWidgetProvider {
    private static final String TAG = "MyAppWidget";

    // need to set the mutability explicitly from Android 12 (but the IMMUTABLE flag is only from SDK version M)
    // https://developer.android.com/about/versions/12/behavior-changes-12#exported
    public static final int IMMUTABLE_INTENT_FLAG = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0);
    public static final int PENDING_INTENT_FLAG = PendingIntent.FLAG_UPDATE_CURRENT | IMMUTABLE_INTENT_FLAG;

    static final String CONFIG_ACTIVITY_TRIGGER = "config_activity";
    static final String ON_UPDATE_TRIGGER = "on_update";

    static final boolean useKeepAliveWorkaround = false;
    static final boolean doNotRescheduleOnUpdate = true;

    static void setupAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.my_app_widget);

        // add pending intent to open config activity on clicking widget
        PendingIntent configPendingIntent = getConfigPendingIntent(context, appWidgetId);
        views.setOnClickPendingIntent(R.id.widget, configPendingIntent);

        // Instruct the widget manager to update the widget to complete basic setup
        appWidgetManager.updateAppWidget(appWidgetId, views);

        // and now update the text part
        updateAppWidget(context, appWidgetManager, appWidgetId, "TEST");
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId, CharSequence widgetText) {
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.my_app_widget);
        views.setTextViewText(R.id.appwidget_text, widgetText);

        // Instruct the widget manager to partially update the widget with new text
        appWidgetManager.partiallyUpdateAppWidget(appWidgetId, views);
    }

    static protected PendingIntent getConfigPendingIntent(Context context, int appWidgetId) {
        context = context.getApplicationContext();
        String ACTION_WIDGET_CONFIGURE = "ConfigureWidget";
        Intent intent = new Intent(context, MyAppWidgetConfigureActivity.class);
        intent.setAction(ACTION_WIDGET_CONFIGURE);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        return PendingIntent.getActivity(context, appWidgetId, intent, PENDING_INTENT_FLAG);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Log.d(TAG, "onUpdate");
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            initialiseAppWidget(context, appWidgetManager, appWidgetId, ON_UPDATE_TRIGGER, 0);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        Log.d(TAG, "onDeleted");
        for (int appWidgetId : appWidgetIds) {
            // clear any pending work...
            MyWorker.clearWorkQueueByWidget(context, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d(TAG, "onEnabled");
        // Enter relevant functionality for when the first widget is created
        if (useKeepAliveWorkaround) {
            MyWorker.scheduleKeepAlive(context);
        }
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.d(TAG, "onDisabled");
        // Enter relevant functionality for when the last widget is disabled
        if (useKeepAliveWorkaround) {
            MyWorker.removeKeepAlive(context);
        }
    }

    static void initialiseAppWidget(Context context, int appWidgetId, String trigger, long delay) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        initialiseAppWidget(context, appWidgetManager, appWidgetId, trigger, delay);
    }

    static void initialiseAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId, String trigger, long delay) {
        // setup widget basics...
        setupAppWidget(context, appWidgetManager, appWidgetId);
        // schedule periodic work to update app widget on a periodic schedule...
        if (doNotRescheduleOnUpdate && trigger.equals(ON_UPDATE_TRIGGER)) {
            Log.d(TAG, "not initialising widget " + appWidgetId + " after onUpdate trigger");
            return;
        }
        MyWorker.schedulePeriodicWork(context, appWidgetId, delay);
    }
}

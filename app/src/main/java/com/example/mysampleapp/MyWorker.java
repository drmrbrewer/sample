package com.example.mysampleapp;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.util.Log;

import com.google.common.util.concurrent.ListenableFuture;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ListenableWorker;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkerParameters;

public class MyWorker extends ListenableWorker {
    private static final String TAG = "MyWorker";

    final private static String METHOD = "method";
    final private static String PERIODIC = "periodic";
    final private static String ONETIME = "onetime";
    final private static String ORIENTATION = "orientation";
    final private static String NAME_KEEP_ALIVE = "keep_alive";

    // various flags to change behaviour...
    final private static boolean doImmediateWork = true;
    final private static boolean doImmediateWorkViaWorkManager = true;
    final private static boolean schedulePeriodicWork = true;
    final private static boolean completeWorkWhenDone = true;
    final private static boolean retryWorkWhenDone = false;
    // this was only available from WorkManager v 2.9.0...
    final private static boolean useNextScheduleTimeOverride = true;
    // else we use "initial delay", which itself was only available from WorkManager v 2.1.0...

    final private static int workDurationSecs = 10;

    public MyWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }

    @Override @NonNull
    public ListenableFuture<Result> startWork() {
        Log.d(TAG, "starting work " + getId());
        return CallbackToFutureAdapter.getFuture(completer -> {
            Data inputData = getInputData();
            int appWidgetId = inputData.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            int orientation = inputData.getInt(ORIENTATION, Configuration.ORIENTATION_UNDEFINED);
            String method = getString(inputData, METHOD);
            Log.d(TAG, "startWork (appWidgetId = " + appWidgetId + ") (method = " + method + ") (orientation = " + orientation + ")");
            doSomeWork(getApplicationContext(), appWidgetId, method, completer);
            return "doing work";
        });
    }

    @Override
    public void onStopped() {
        Log.d(TAG, "onStopped");
    }

    private static void scheduleWork(Context context, int appWidgetId) {
        scheduleWork(context, appWidgetId, null);
    }
    private static void scheduleWork(Context context, int appWidgetId, Long delay) {
        updateWidgetText(context, appWidgetId, "schedule onetime work");
        Log.d(TAG, "scheduleWork for " + appWidgetId);

        WorkManager workManager = getWorkManager(context);
        if (workManager == null) {
            return;
        }

        delay = (delay != null) ? delay : 0;
        Constraints constraints = getConstraints(false);
        Data inputData = getOneTimeInputData(appWidgetId);
        OneTimeWorkRequest myWork = getOneTimeWorkRequest(appWidgetId, delay, constraints, inputData);
        String name = getOneTimeName(appWidgetId);
        Log.d(TAG, "enqueueing OneTimeWorkRequest for widget " + appWidgetId + " with name " + name + " and id " + myWork.getId());
        workManager.enqueueUniqueWork(name, ExistingWorkPolicy.REPLACE, myWork);
    }

    static void schedulePeriodicWork(Context context, int appWidgetId, long delay) {
        Log.d(TAG, "schedulePeriodicWork BEFORE (appWidgetId = " + appWidgetId + ")");

        if (doImmediateWork) {
            // do an immediate update, with periodic work kicking in later (subject to Doze etc)
            if (doImmediateWorkViaWorkManager) {
                // schedule it as a onetime work...
                scheduleWork(context, appWidgetId);
            } else {
                // or call it directly...
                doSomeWork(context, appWidgetId);
            }
        }

        if (!schedulePeriodicWork) {
            return;
        }

        WorkManager workManager = getWorkManager(context);
        if (workManager == null) {
            return;
        }

        long interval = 15 * 60 * 1000; // 15 mins

        Constraints constraints = getConstraints(true);
        Data inputData = getPeriodicInputData(appWidgetId);
        PeriodicWorkRequest myWork = getPeriodicWorkRequest(context, appWidgetId, interval, delay, constraints, inputData);
        String name = getPeriodicName(appWidgetId);
        Log.d(TAG, "enqueueing PERIODIC work request for widget " + appWidgetId + " with name " + name + " and id " + myWork.getId());
        workManager.enqueueUniquePeriodicWork(name, ExistingPeriodicWorkPolicy.UPDATE, myWork);
        // log this info after enqueuing to get sensible work info...
        logWorkInfo(context, appWidgetId);
    }

    private static void logWorkInfo(Context context, int appWidgetId) {
        WorkInfo wi = getWorkInfoByWidget(context, appWidgetId);
        if (wi == null) {
            Log.d(TAG, "logWorkInfo (appWidgetId = " + appWidgetId + ") no work info available");
        } else {
            Log.d(TAG, "logWorkInfo (appWidgetId = " + appWidgetId + ") (generation: " + wi.getGeneration() + ") (nextScheduleTimeMillis: " + wi.getNextScheduleTimeMillis() + ")");
        }
    }

    private static Constraints getConstraints(boolean needConstraints) {
        Log.d(TAG, "getConstraints");
        Constraints.Builder constraintsBuilder = new Constraints.Builder();
        if (needConstraints) {
            Log.d(TAG, "adding network constraint");
            NetworkType networkType = NetworkType.CONNECTED;
            constraintsBuilder.setRequiredNetworkType(networkType);
        }
        return needConstraints ? constraintsBuilder.build() : Constraints.NONE;
    }

    private static OneTimeWorkRequest getOneTimeWorkRequest(int appWidgetId, long delay, Constraints constraints, Data inputData) {
        Log.d(TAG, "getOneTimeWorkRequest for widget " + appWidgetId);

        String workTag = getWorkTag(appWidgetId);

        Log.d(TAG, "creating OneTimeWorkRequest with tag " + workTag + " with initial delay " + delay + " ms");
        OneTimeWorkRequest.Builder workRequestBuilder = new OneTimeWorkRequest.Builder(MyWorker.class);
        if (delay > 0) {
            workRequestBuilder.setInitialDelay(delay, TimeUnit.MILLISECONDS);
        }
        workRequestBuilder.setConstraints(constraints);
        workRequestBuilder.setInputData(inputData);
        workRequestBuilder.addTag(workTag);
        workRequestBuilder.addTag(ONETIME);
        return workRequestBuilder.build();
    }

    private static PeriodicWorkRequest getPeriodicWorkRequest(Context context, int appWidgetId, long interval, long delay, Constraints constraints, Data inputData) {
        Log.d(TAG, "getPeriodicWorkRequest for widget " + appWidgetId);

        String workTag = getWorkTag(appWidgetId);

        long firstDelay = interval + delay;
        Log.d(TAG, "delaying first periodic work by " + firstDelay + " ms");
        String timeStr = getTimeStr(firstDelay);
        updateWidgetText(context, appWidgetId, "schedule periodic work (first period starts at " + timeStr + ")");

        Log.d(TAG, "creating PeriodicWorkRequest with tag " + workTag + " and with interval " + interval + " ms");
        PeriodicWorkRequest.Builder workRequestBuilder = new PeriodicWorkRequest.Builder(MyWorker.class, interval, TimeUnit.MILLISECONDS);
        if (useNextScheduleTimeOverride) {
            Log.d(TAG, "setting next schedule override time on periodic");
            workRequestBuilder.setNextScheduleTimeOverride(System.currentTimeMillis() + firstDelay);
        } else {
            Log.d(TAG, "setting initial delay on periodic");
            workRequestBuilder.setInitialDelay(firstDelay, TimeUnit.MILLISECONDS);
        }
        workRequestBuilder.setConstraints(constraints);
        workRequestBuilder.setInputData(inputData);
        workRequestBuilder.addTag(workTag);
        workRequestBuilder.addTag(PERIODIC);
        // https://developer.android.com/topic/libraries/architecture/workmanager/how-to/define-work#retry_and_backoff_policy
        workRequestBuilder.setBackoffCriteria(
                BackoffPolicy.EXPONENTIAL,
                1,
                TimeUnit.MINUTES);
        return workRequestBuilder.build();
    }

    private static Data getOneTimeInputData(int appWidgetId) {
        Log.d(TAG, "getOneTimeInputData for widget " + appWidgetId);
        Data.Builder dataBuilder = new Data.Builder();
        dataBuilder.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        dataBuilder.putInt(ORIENTATION, 1);
        dataBuilder.putString(METHOD, ONETIME);
        return dataBuilder.build();
    }

    private static Data getPeriodicInputData(int appWidgetId) {
        Log.d(TAG, "creating Data for widget " + appWidgetId);
        Data.Builder dataBuilder = new Data.Builder();
        dataBuilder.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        dataBuilder.putInt(ORIENTATION, 1);
        dataBuilder.putString(METHOD, PERIODIC);
        return dataBuilder.build();
    }

    private static WorkManager getWorkManager(Context context) {
        WorkManager workManager;
        try {
            workManager = WorkManager.getInstance(context.getApplicationContext());
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return null;
        }
        return workManager;
    }

    private static String getWorkTag(int appWidgetId) {
        return "widget-" + appWidgetId;
    }

    private static String getOneTimeName(int appWidgetId) {
        return "unique-" + appWidgetId;
    }

    private static String getPeriodicName(int appWidgetId) {
        return "periodic-" + appWidgetId;
    }

    private static String getString(Data data, String key) {
        String str = data.getString(key);
        return str == null ? "unknown" : str;
    }

    static void clearWorkQueueByWidget(Context context, int appWidgetId) {
        WorkManager workManager = getWorkManager(context);
        if (workManager == null) {
            return;
        }
        String workTag = getWorkTag(appWidgetId);
        clearWorkQueueByTag(workManager, workTag);
    }

    private static void clearWorkQueueByTag(WorkManager workManager, String tag) {
        ListenableFuture<List<WorkInfo>> workInfos = workManager.getWorkInfosByTag(tag);
        clearWorkQueue(workManager, workInfos);
    }

    private static void clearWorkQueue(WorkManager workManager, ListenableFuture<List<WorkInfo>> workInfos) {
        Log.d(TAG, "clearWorkQueue");
        if (workInfos == null) { return; }
        // based on https://stackoverflow.com/a/51613101/4070848
        try {
            // although workInfos is a ListenableFuture and hence by design asynchronous
            // I think .get() effectively is synchronous... blocks until it gets the list
            // see https://stackoverflow.com/a/39295152/4070848
            boolean cancelThisOne;
            List<WorkInfo> workInfoList = workInfos.get();
            if (workInfoList == null) { return; }
            for (WorkInfo workInfo : workInfoList) {
                if (workInfo == null) { continue; }
                Log.d(TAG, "workInfo: " + workInfo);
                WorkInfo.State state = workInfo.getState();
                cancelThisOne = (state == WorkInfo.State.ENQUEUED);
                if (cancelThisOne) {
                    UUID workerId = workInfo.getId();
                    Log.d(TAG, "cancelling work with id " + workerId);
                    workManager.cancelWorkById(workerId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void doSomeWork(final Context context, final int appWidgetId) {
        doSomeWork(context, appWidgetId, null, null);
    }
    private static void doSomeWork(final Context context, final int appWidgetId, String method, final CallbackToFutureAdapter.Completer<ListenableWorker.Result> completer) {
        Log.d(TAG, "starting some imaginary work");
        String type = method == null ? "direct" : method;
        updateWidgetText(context, appWidgetId, "doing " + type + " work (takes " + workDurationSecs + " secs)");
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            Log.d(TAG, "finished the imaginary work");
            // Toast.makeText(context, "finished some work", Toast.LENGTH_LONG).show();
            String msg = type + " work completed";
            if (completer == null) {
                Log.d(TAG, "completer is NULL");
            } else {
                if (completeWorkWhenDone) {
                    Result result = retryWorkWhenDone ? Result.retry() : Result.success();
                    msg += " (and completer set to " + (retryWorkWhenDone ? "Retry" : "Success") + ")";
                    Log.d(TAG, "setting completer to success state");
                    completer.set(result);
                } else {
                    // simulate some problem in doing the work which means that completed is never set...
                    msg += " (but completer not set)";
                    Log.d(TAG, "NOT setting completer to success state");
                }
            }
            updateWidgetText(context, appWidgetId, msg);

        }, workDurationSecs * 1000);
    }

    private static void updateWidgetText(final Context context, final int appWidgetId, String msg) {
        String timeStr = getTimeStr(0);
        msg = timeStr + " - " + msg;
        Log.d(TAG, "showing message " + msg);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        MyAppWidget.updateAppWidget(context, appWidgetManager, appWidgetId, msg);
    }

    private static String getTimeStr(long offsetMs) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MILLISECOND, (int) offsetMs);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(cal.getTime());
    }

    private static WorkInfo getWorkInfoByWidget(Context context, int appWidgetId) {
        // based on https://stackoverflow.com/a/51613101/4070848
        String tag = getWorkTag(appWidgetId);
        WorkManager workManager = getWorkManager(context);
        if (workManager == null) {
            return null;
        }
        ListenableFuture<List<WorkInfo>> workInfos = workManager.getWorkInfosByTag(tag);
        try {
            // although workInfos is a ListenableFuture and hence by design asynchronous
            // I think .get() effectively is synchronous... blocks until it gets the list
            // see https://stackoverflow.com/a/39295152/4070848
            List<WorkInfo> workInfoList = workInfos.get();
            if (workInfoList == null) { return null; }
            for (WorkInfo workInfo : workInfoList) {
                Log.d(TAG, "workInfo: " + workInfo);
                WorkInfo.State state = workInfo.getState();
                if (state == WorkInfo.State.ENQUEUED || state == WorkInfo.State.RUNNING) {
                    // note that not all values will make sense if in a RUNNING state... e.g. nextScheduleTimeMillis
                    return workInfo;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    static void scheduleKeepAlive(Context context) {
        // based on:
        //    https://issuetracker.google.com/issues/115575872#comment16
        //    https://stackoverflow.com/a/70685721/4070848
        WorkManager workManager = getWorkManager(context);
        if (workManager == null) {
            return;
        }
        OneTimeWorkRequest.Builder workRequestBuilder = new OneTimeWorkRequest.Builder(MyWorker.class);
        long buffer = 365 * 24L * 60 * 60 * 1000;
        long maxDelay = (Long.MAX_VALUE - System.currentTimeMillis() - buffer);
        Log.d(TAG, "scheduleKeepAlive with delay " + maxDelay);
        workRequestBuilder.setInitialDelay(maxDelay, TimeUnit.MILLISECONDS);
        workRequestBuilder.addTag(NAME_KEEP_ALIVE);
        OneTimeWorkRequest keepAliveWork = workRequestBuilder.build();
        workManager.enqueueUniqueWork(NAME_KEEP_ALIVE, ExistingWorkPolicy.KEEP, keepAliveWork);
    }

    static void removeKeepAlive(Context context) {
        // see note above...
        WorkManager workManager = getWorkManager(context);
        if (workManager == null) {
            return;
        }
        Log.d(TAG, "removeKeepAlive");
        workManager.cancelUniqueWork(NAME_KEEP_ALIVE);
    }
}
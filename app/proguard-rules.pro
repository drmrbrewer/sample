# Add project specific ProGuard rules here.

# to suppress warnings related to the use of the guava library
# see https://stackoverflow.com/a/17988163/4070848
-dontwarn com.google.**
# and also the following when I upgraded from guava 23.4-android to 23.6-android
# see https://stackoverflow.com/a/47555897/4070848
-dontwarn afu.org.checkerframework.**
-dontwarn org.checkerframework.**

# to get rid of lots of (non fatal) errors when building the apk in Android 2.2
 # see https://stackoverflow.com/a/39131672/4070848
-keepattributes EnclosingMethod

# Following is to prevent the methods in my WebAppInterface from being wiped out on generating
# the apk (with minification... only happens with release version not dev version) because
# to proguard the methods appear as if they are never called -- but they ARE called... from the js
# See:
# https://stackoverflow.com/a/17637530/4070848
# https://stackoverflow.com/a/30363458/4070848
-keepattributes JavascriptInterface
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# Remove all Log.d() etc calls for the release version
# http://proguard.sourceforge.net/manual/examples.html#logging
# note: to make this work, in build.gradle you also need to refer to 'proguard-android-optimize.txt' rather than 'proguard-android.txt'
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}

# to put the mapping (deobfuscation) file into the same folder as this, so that it ends up in bitbucket (to use in Dev Console to debug)
# https://stackoverflow.com/a/29141711/4070848
# xxx not doing anything though?
-printmapping mapping.txt